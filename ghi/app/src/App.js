import Nav from "./Nav";
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import AttendConferenceForm from "./AttendConferenceForm";
import { createBrowserRouter, RouterProvider, BrowserRouter, Routes, Route, Outlet } from "react-router-dom"
import PresentationForm from "./PresentationForm";
import MainPage from "./MainPage";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }

  const router = createBrowserRouter([
    {
      path: "/",
      element: (
        <>
          <Nav />
          <div className="container">
            <Outlet />
          </div>
        </>
      ),
      children: [
        {
          index: true, element: <MainPage />
        },
        {
          path: "locations",
          children: [
            { path: "new", element: <LocationForm />}
          ]
        },
        {
          path: "conferences",
          children: [
            {path: "new", element: <ConferenceForm />}
          ]
        },
        {
          path: "attendees",
          element: <Outlet />,
          children: [
            {index: true, element: <AttendeesList attendees={props.attendees} />},
            {path: "new", element: <AttendConferenceForm />}
          ]
        },
        {
          path: "presentations",
          children: [
            {path: "new", element: <PresentationForm />}
          ]
        }
      ]
    },
  ])
  return <RouterProvider router={router} />
}

export default App;
