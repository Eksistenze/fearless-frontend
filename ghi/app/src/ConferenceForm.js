import React, { useEffect, useState } from 'react'

function ConferenceForm() {
    const [locations, setLocations] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)

        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const [name, setName] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value)
    }

    const [startDate, setStartDate] = useState('');

    const handleStartChange = (event) => {
        const value = event.target.value;
        setStartDate(value)
    }

    const [endDate, setEndDate] = useState('');

    const handleEndChange = (event) => {
        const value = event.target.value;
        setEndDate(value)
    }

    const [description, setDescription] = useState('')

    const handleDescChange = (event) => {
        const value = event.target.value;
        setDescription(value)
    }

    const [presentations, setPresentations] = useState('')

    const handlePresentationsChange = (event) => {
        const value = event.target.value;
        setPresentations(value)
    }

    const [attendees, setAttendees] = useState('')

    const handleAttendeesChange = (event) => {
        const value = event.target.value;
        setAttendees(value)
    }

    const [location, setLocation] = useState('')

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.starts = startDate;
        data.ends = endDate;
        data.description = description;
        data.max_presentations = presentations;
        data.max_attendees = attendees;
        data.location = location;

        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();

            setName('');
            setStartDate('');
            setEndDate('');
            setDescription('');
            setPresentations('');
            setAttendees('');
            setLocation('');
        }
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartChange} value={startDate} placeholder="mm/dd/yyyy" required type="date" name="starts" id="starts" className="form-control"/>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndChange} value={endDate} placeholder="mm/dd/yyyy" required type="date" name="ends" id="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="form-floating mb-3">
                <textarea onChange={handleDescChange} value={description} required placeholder="" style={{height: 109}} name="description" id="description" className="form-control"></textarea>
                <label htmlFor="description">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePresentationsChange} value={presentations} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                <label htmlFor="max_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleAttendeesChange} value={attendees} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                     return (
                         <option key={location.id} value={location.id}>
                             {location.name}
                         </option>
                     )
                   })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    // {/* <div className="row">
    //     <div className="offset-3 col-6">
    //       <div className="shadow p-4 mt-4">
    //         <h1>Create a new location</h1>
    //         <form onSubmit={handleSubmit}id="create-location-form">
    //           <div className="form-floating mb-3">
    //             <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
    //             <label htmlhtmlFor="name">Name</label>
    //           </div>
    //           <div className="form-floating mb-3">
    //             <input onChange={handleRoomChange} value={roomCount} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control"/>
    //             <label htmlhtmlFor="room_count">Room count</label>
    //           </div>
    //           <div className="form-floating mb-3">
    //             <input onChange={handleCityChange} value={city} placeholder="City" required type="text" name="city" id="city" className="form-control"/>
    //             <label htmlhtmlFor="city">City</label>
    //           </div>
    //           <div className="mb-3">
    //             <select onChange={handleStateChange} value={state} required name="state" id="state" className="form-select">
    //               <option value="">Choose a state</option>
    //               {states.map(state => {
    //                 return (
    //                     <option key={state.abbreviation} value={state.abbreviation}>
    //                         {state.name}
    //                     </option>
    //                 )
    //               })}
    //             </select>
    //           </div>
    //           <button className="btn btn-primary">Create</button>
    //         </form>
    //       </div>
    //     </div>
    //   </div> */}
    );
}

export default ConferenceForm;
