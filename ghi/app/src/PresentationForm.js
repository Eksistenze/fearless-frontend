import React, { useEffect, useState } from 'react'


function PresentationForm() {
    const [conferences, setConferences] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences)

        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const [presName, setPresName] = useState('');

    const handlePresNameChange = (event) => {
        const value = event.target.value;
        setPresName(value)
    }

    const [email, setEmail] = useState('');

    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value)
    }

    const [compName, setCompName] = useState('');

    const handleCompNameChange = (event) => {
        const value = event.target.value;
        setCompName(value)
    }

    const [title, setTitle] = useState('')

    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value)
    }

    const [synopsis, setSynopsis] = useState('')

    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value)
    }

    const [conference, setConference] = useState('')

    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.presenter_name = presName;
        data.presenter_email = email;
        data.company_name = compName;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference;

        const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setPresName('');
            setEmail('');
            setCompName('');
            setTitle('');
            setSynopsis('');
            setConference('');
        }
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handlePresNameChange} value={presName} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"></input>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEmailChange} value={email} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"></input>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompNameChange} value={compName} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"></input>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTitleChange} value={title} placeholder="Title" required type="text" name="title" id="title" className="form-control"></input>
                <label htmlFor="title">Title</label>
              </div>
              <div className="form-floating mb-3">
                <textarea onChange={handleSynopsisChange} value={synopsis} placeholder="" required style={{ height: 109 }} name="synopsis" id="synopsis" className="form-control"></textarea>
                <label htmlFor="synopsis">Synopsis</label>
              </div>
              <div className="mb-3">
                <select onChange={handleConferenceChange} value={conference} required name="conference" id="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                    return (
                        <option key={conference.id} value={conference.id}>
                            {conference.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>


    );
}


export default PresentationForm
