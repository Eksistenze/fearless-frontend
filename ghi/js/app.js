function createCard(title, description, pictureUrl, dateRange, location) {
    return `
      <div class="card shadow p-3 mb-5 w-25 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
          ${dateRange}
        </div>
      </div>
    `;
  }

function dateFormat(date) {
    const day = date.getDate();
    const month = date.getMonth();
    const year = date.getFullYear();
    return `${month}/${day}/${year}`
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences';
    try {
    const response = await fetch(url);

    if (!response.ok) {
        throw new Error('Response not ok');
    } else {

    const data = await response.json();

    //
    for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = new Date(details.conference.starts);
          const endDate = new Date(details.conference.ends);
          const dateRange = `${dateFormat(startDate)} - ${dateFormat(endDate)}`;
          const location = details.conference.location.name;
          const html = createCard(title, description, pictureUrl, dateRange, location);
          const column = document.querySelector('.col');

          column.innerHTML += html
        }
    } }
    } catch (e) {
        console.error('error', e)
    }
});
