function createOption(locationName, locationId) {
    return `
    <option value="${locationId}">${locationName}</option>`
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';
    try {
    const response = await fetch(url);

    if (!response.ok) {
        throw new Error('Response not ok');
    } else {

    const data = await response.json();

    //
    for (let location of data.locations) {
        const locationName = location.name
        const locationId = location.id
        const html = createOption(locationName, locationId)
        const option = document.getElementById('location')
        option.innerHTML += html
    } }
    } catch (e) {
        console.error('error', e)
    }

    const formTag = document.getElementById('create-conference-form')
    formTag.addEventListener('submit', async event => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))

        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: 'POST',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(conferenceUrl, fetchConfig)
        if (response.ok) {
            formTag.reset()
            const newConference = await response.json()
            console.log(newConference);
        }
    })
});
