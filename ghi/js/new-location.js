function createOption(stateName, abbreviation) {
    return `
    <option value="${abbreviation}">${stateName}</option>`
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/';
    try {
    const response = await fetch(url);

    if (!response.ok) {
        throw new Error('Response not ok');
    } else {

    const data = await response.json();

    //
    for (let state of data.states) {
        const stateName = state.name
        const abbreviation = state.abbreviation
        const html = createOption(stateName, abbreviation)
        const option = document.getElementById('state')
        option.innerHTML += html
    } }
    } catch (e) {
        console.error('error', e)
    }

    const formTag = document.getElementById('create-location-form')
    formTag.addEventListener('submit', async event => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))

        const locationUrl = 'http://localhost:8000/api/locations/'
        const fetchConfig = {
            method: 'POST',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(locationUrl, fetchConfig)
        if (response.ok) {
            formTag.reset()
            const newLocation = await response.json()
            console.log(newLocation);
        }
    })
});
