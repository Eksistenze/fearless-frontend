function createOption(conferenceName, conferenceId) {
    return `
    <option value="${conferenceId}">${conferenceName}</option>`
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences';
    try {
    const response = await fetch(url);

    if (!response.ok) {
        throw new Error('Response not ok');
    } else {

    const data = await response.json();

    //
    for (let conference of data.conferences) {
        const conferenceName = conference.name
        const conferenceId = conference.id
        const html = createOption(conferenceName, conferenceId)
        const option = document.getElementById('conference')
        option.innerHTML += html
    } }
    } catch (e) {
        console.error('error', e)
    }

    const formTag = document.getElementById('create-presentation-form')
    formTag.addEventListener('submit', async event => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))
        const conferenceTag = document.getElementById('conference')
        const conference = conferenceTag.options[conferenceTag.selectedIndex].value
        const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`
        const fetchConfig = {
            method: 'POST',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        }
        try{
        const response = await fetch(presentationUrl, fetchConfig)
        if (response.ok) {
            formTag.reset()
            const newPresentation = await response.json()
        } else {
            throw new Error('Response not ok');
        }
    } catch (e) {
        console.error('error', e)
    }
    })
});
